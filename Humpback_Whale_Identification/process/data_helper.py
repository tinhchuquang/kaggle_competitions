import pandas as pd

PJ_DR = r'/home/tinhcq/Humpback_Whale_Identification'
train_df = pd.read_csv('/data/Humpback_Whale_Identification/train.csv')
TRN_IMGS_DIR = '/data/Humpback_Whale_Identification/train'
TST_IMGS_DIR = '/data/Humpback_Whale_Identification/whale/test'
LIST_DIR = PJ_DR + r'image_list'


def load_label_dict(label_list_path):
    f = open(label_list_path, 'r')
    lines = f.readlines()
    f.close()

    label_dict = {}
    for line in lines:
        line = line.strip()
        line = line.split(' ')
        id = line[0]
        index = int(line[1])
        label_dict[id] = index

    return label_dict


def get_list(csv):
    bbox_data = pd.read_csv(csv)
    labelName = bbox_data['Image'].tolist()
    x0 = bbox_data['x0'].tolist()
    y0 = bbox_data['y0'].tolist()
    x1 = bbox_data['x1'].tolist()
    y1 = bbox_data['y1'].tolist()

    return labelName, x0, y0, x1, y1


def read_txt(txt):
    f = open(txt, 'r')
    lines = f.readlines()
    f.close()
    lines = [tmp.strip() for tmp in lines]

    list = []
    for line in lines:
        line = line.split(' ')
        list.append([line[0], int(line[1])])

    return list


def load_train_list(train_image_list_path=LIST_DIR + r'train_image_list.txt'):
    f = open(train_image_list_path, 'r')
    lines = f.readlines()
    f.close()

    list = []
    for line in lines:
        line = line.strip()
        line = line.split(' ')
        img_name = line[0]
        index = int(line[1])
        list.append([img_name, index])
    return list