import keras
from math import floor
from my_generator import TestDataGenerator, TrainDataGenerator

def _initial_layer(input_dims):
    inputs = keras.layers.Input(input_dims)
    x = keras.layers.Conv2D(filters=3, kernel_size=(1, 1), strides=(1, 1), name='initial_conv2d')(inputs)
    x = keras.layers.BatchNormalization(axis=3, epsilon=1.001e-5, name='initial_bn')(x)
    x = keras.layers.Activation('relu', name='initial_relu')(x)

    return keras.models.Model(inputs, x)


class MyDeepModel:
    def __init__(self, engine, input_dims, batch_size=5, learning_rate=1e-3, decay_rate=1.0, decay_steps=1,
                 weights="imagenet",
                 verbose=1):
        self.engine = engine
        self.input_dims = input_dims
        self.batch_size = batch_size
        self.learning_rate = learning_rate
        self.decay_rate = decay_rate
        self.decay_steps = decay_steps
        self.weights = weights
        self.verbose = verbose
        self._build()

    def _build(self):
        initial_layer = _initial_layer((*self.input_dims, 1))

        engine = self.engine(include_top=False, weights=self.weights, input_shape=(*self.input_dims, 3),
                             backend=keras.backend, layers=keras.layers,
                             models=keras.models, utils=keras.utils)

        x = engine(initial_layer.output)

        x = keras.layers.GlobalAveragePooling2D(name='avg_pool')(x)

        out = keras.layers.Dense(6, activation="sigmoid", name='dense_output')(x)

        self.model = keras.models.Model(inputs=initial_layer.input, outputs=out)

        self.model.compile(loss="binary_crossentropy", optimizer=keras.optimizers.Adam(0.0))

    def train(self, df, train_idx, img_dir, global_epoch):
        self.model.fit_generator(
            TrainDataGenerator(
                df.iloc[train_idx].index,
                df.iloc[train_idx],
                self.batch_size,
                self.input_dims,
                img_dir
            ),
            verbose=self.verbose,
            use_multiprocessing=True,
            workers=4,
            epochs=1,
            callbacks=[
                keras.callbacks.LearningRateScheduler(
                    lambda epoch: self.learning_rate * pow(self.decay_rate, floor(global_epoch / self.decay_steps))
                )
            ]
        )

    def test(self, df, test_idx, img_dir):
        predictions = \
            self.model.predict_generator(
                TestDataGenerator(
                    df.iloc[test_idx].index,
                    None,
                    self.batch_size,
                    self.input_dims,
                    img_dir
                ),
                verbose=1,
                use_multiprocessing=True,
                workers=4
            )

        return predictions[:df.iloc[test_idx].shape[0]]

    def save(self, path):
        self.model.save_weights(path)

    def load(self, path):
        self.model.load_weights(path)
