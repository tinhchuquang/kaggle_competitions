import cv2
import numpy as np
import pydicom


def _get_first_of_dicom_field_as_int(x):
    if type(x) == pydicom.multival.MultiValue:
        return int(x[0])
    else:
        return int(x)


def _get_windowing(data):
    dicom_fields = [data.WindowCenter, data.WindowWidth, data.RescaleSlope, data.RescaleIntercept]
    return [_get_first_of_dicom_field_as_int(x) for x in dicom_fields]


def _window_image(img, window_center, window_width, slope, intercept):
    img = (img * slope + intercept)
    img_min = window_center - window_width // 2
    img_max = window_center + window_width // 2
    img[img < img_min] = img_min
    img[img > img_max] = img_max
    return img


def _normalize(img):
    if img.max() == img.min():
        return np.zeros(img.shape)
    return 2 * (img - img.min()) / (img.max() - img.min()) - 1


def _read(path, desired_size=(224, 224)):
    """Will be used in DataGenerator"""

    dcm = pydicom.dcmread(path)

    window_params = _get_windowing(dcm)  # (center, width, slope, intercept)

    try:
        # dcm.pixel_array might be corrupt (one case so far)
        img = _window_image(dcm.pixel_array, *window_params)
    except:
        img = np.zeros(desired_size)

    img = _normalize(img)

    if desired_size != (512, 512):
        # resize image
        img = cv2.resize(img, desired_size, interpolation=cv2.INTER_LINEAR)

    return img[:, :, np.newaxis]
