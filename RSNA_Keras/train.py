import numpy as np
import pandas as pd
from keras_applications.resnet import ResNet50
from sklearn.model_selection import ShuffleSplit

from models import MyDeepModel


def read_testset(filename='/data/rsna-intracranial-hemorrhage-detection/stage_1_sample_submission.csv'):
    df = pd.read_csv(filename)
    df['Image'] = df['ID'].str.slice(stop=12)
    df["Diagnosis"] = df["ID"].str.slice(start=13)

    df = df.loc[:, ["Label", "Diagnosis", "Image"]]
    df = df.set_index(['Image', 'Diagnosis']).unstack(level=-1)

    return df


def read_trainset(filename='/data/rsna-intracranial-hemorrhage-detection/stage_1_train.csv'):
    df = pd.read_csv(filename)
    df["Image"] = df["ID"].str.slice(stop=12)
    df["Diagnosis"] = df["ID"].str.slice(start=13)

    duplicates_to_remove = [
        1598538, 1598539, 1598540, 1598541, 1598542, 1598543,
        312468, 312469, 312470, 312471, 312472, 312473,
        2708700, 2708701, 2708702, 2708703, 2708704, 2708705,
        3032994, 3032995, 3032996, 3032997, 3032998, 3032999
    ]

    df = df.drop(index=duplicates_to_remove)
    df = df.reset_index(drop=True)

    df = df.loc[:, ["Label", "Diagnosis", "Image"]]
    df = df.set_index(['Image', 'Diagnosis']).unstack(level=-1)

    return df


test_df = read_testset()
df = read_trainset()

_TEST_IMAGES = '/data/rsna-intracranial-hemorrhage-detection/stage_1_test_images/'
_TRAIN_IMAGES = '/data/rsna-intracranial-hemorrhage-detection/stage_1_train_images/'


def run(model, df, train_idx, valid_idx, test_df, epochs):
    global_loss = np.inf
    valid_predictions = []
    test_predictions = []
    for global_epoch in range(epochs):
        print('Global epoch: ', global_epoch)
        model.train(df, train_idx, _TRAIN_IMAGES, global_epoch)

        test_predictions.append(model.test(test_df, range(test_df.shape[0]), _TEST_IMAGES))
        valid_predictions.append(model.test(df, valid_idx, _TRAIN_IMAGES))

        model.save('resnet50/models.h5')
        loss = weighted_loss_metric(df.iloc[valid_idx].values,
                                    np.average(valid_predictions, axis=0,
                                               weights=[2 ** i for i in range(len(valid_predictions))]))
        if global_loss > loss:
            model.save('resnet50/best_models.h5')
            global_loss = loss
        print("validation loss: %.4f" % loss)
    return test_predictions, valid_predictions


def weighted_loss_metric(trues, preds, weights=[0.2, 0.1, 0.1, 0.1, 0.1, 0.1], clip_value=1e-7):
    """this is probably not correct, but works OK. Feel free to give feedback."""
    preds = np.clip(preds, clip_value, 1 - clip_value)
    loss_subtypes = trues * np.log(preds) + (1 - trues) * np.log(1 - preds)
    loss_weighted = np.average(loss_subtypes, axis=1, weights=weights)
    return - loss_weighted.mean()


# train set (90%) and validation set (10%)
ss = ShuffleSplit(n_splits=5, test_size=0.1, random_state=42).split(df.index)

# will just do one fold
train_idx, valid_idx = next(ss)

# obtain model
model = MyDeepModel(engine=ResNet50, input_dims=(224, 224), batch_size=32, learning_rate=1e-3,
                    decay_rate=0.75, decay_steps=1, weights="imagenet", verbose=2)

# run 3 epochs and obtain test + validation predictions
test_preds, _ = run(model, df, train_idx, valid_idx, test_df, 30)
